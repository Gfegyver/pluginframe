package hu.Gfegyver.Frame.Main;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import hu.Gfegyver.Frame.Config.C;

public class Main extends JavaPlugin{
	
	public static Main main;
	private static final boolean DEBUG = true;

	public void onEnable() {
		main = this;
		init();
		registerCustomCommands();
	}

	private void init() {
		C.init();
	}

	public static void log(String msg) {
		main.getServer().getConsoleSender().sendMessage("[Frame] " + msg);
	}

	private void registerCustomCommands() {
		//TelekManagerCommand tmc = new TelekManagerCommand();
		//CommandHandler.registerCommand(tmc.getName(), tmc);
	}

	public static boolean debug() {
		return DEBUG;
	}

	public static boolean debug(String debug_msg) {
		if (DEBUG)
			log(ChatColor.RED + "[Debug] " + ChatColor.GREEN + debug_msg);
		return DEBUG;
	}
	
}
