package hu.Gfegyver.Frame.Utils;

import org.bukkit.command.CommandSender;

public abstract class SubCommand {

	private String name;

	public SubCommand(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract boolean dispatch(CommandSender sender, String[] args);

	public abstract void Info(CommandSender sender);

}
