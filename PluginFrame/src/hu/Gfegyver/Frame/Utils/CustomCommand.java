package hu.Gfegyver.Frame.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public abstract class CustomCommand extends BukkitCommand {

	private Map<String, SubCommand> subCommands = new HashMap<>();

	protected CustomCommand(String name) {
		super(name);
	}

	/**
	 * Create a BukkitCommand and a CustomCommand
	 * 
	 * @param name
	 *            - The name of the command
	 * @param description
	 *            - Description of the command which is seen in the /help cmd
	 * @param usage
	 *            - Command usage message. Seen, if the execute return is false
	 * @param aliasses
	 *            - List of alliasses of the command
	 */
	protected CustomCommand(String name, String description, String usage, List<String> aliasses) {
		super(name, description, usage, aliasses);
	}

	@Override
	public abstract boolean execute(CommandSender sender, String alias, String[] args);

	public void addSubCommand(SubCommand sc) {
		subCommands.put(sc.getName(), sc);
	}

	public abstract void info(CommandSender sender);

	protected SubCommand getSubCommand(String subcmd) {
		return subCommands.get(subcmd);
	}

}
