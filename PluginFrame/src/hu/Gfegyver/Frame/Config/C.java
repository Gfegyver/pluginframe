package hu.Gfegyver.Frame.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.bukkit.configuration.file.YamlConfiguration;

import hu.Gfegyver.Frame.Main.Main;

public final class C {

	private static YamlConfiguration config;
	private static File dataFile;

	public static void init() {

		generate();
		update();
		load();
		/*
		 * TODO getters setters
		 */
	}

	/*
	 * static getters and setters for the values
	 */

	
	/*
	 * Load in from Config file
	 */
	private static void load() {
		
	}

	private static void update() {
		try {
			YamlConfiguration def = YamlConfiguration.loadConfiguration(
					getFile(Main.main.getClass().getClassLoader().getResourceAsStream("config.yml")));
			for (String key : def.getKeys(true)) {
				if (!config.contains(key))
					config.set(key, def.get(key));
			}
			save();
		} catch (Exception e) {
		}
	}

	private static void generate() {
		dataFile = new File(Main.main.getDataFolder(), "config.yml");
		if (!dataFile.exists()) {
			try {
				config = YamlConfiguration.loadConfiguration(
						getFile(Main.main.getClass().getClassLoader().getResourceAsStream("config.yml")));
				config.save(dataFile);
			} catch (IOException e) {
			}
		}
		config = YamlConfiguration.loadConfiguration(dataFile);
	}

	private static void save() {
		try {
			config.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static File getFile(InputStream in) throws IOException {
		final File tempFile = File.createTempFile("config", "yml");
		tempFile.deleteOnExit();
		try (FileOutputStream out = new FileOutputStream(tempFile)) {
			IOUtils.copy(in, out);
		}
		return tempFile;
	}

}
