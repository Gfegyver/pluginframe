package hu.Gfegyver.Frame.Handlers;

import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;

import hu.Gfegyver.Frame.Utils.CustomCommand;

public class CommandHandler {

	private static HashMap<String, CustomCommand> handler = new HashMap<>();

	public static boolean containsCommand(String cmd) {
		return handler.containsKey(cmd);
	}

	public static CustomCommand getCommand(String cmd) {
		return handler.get(cmd);
	}

	public static void registerCommand(String cmd, CustomCommand cc) {
		((CraftServer) Bukkit.getServer()).getCommandMap().register(cmd, cc);
		handler.put(cmd, cc);
	}

	public static Set<String> getCommands() {
		return handler.keySet();
	}

	public static void removeCommand(String cmd) {
		handler.remove(cmd);
	}

}
